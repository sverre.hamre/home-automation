/*
  co2 using multiple sensors and reporting the data on an lcd display
*/


#include <Wire.h>    // I2C library
//#include "ens210.h"  // ENS210 library
#include "HDC1080JS.h" //HDC1080 library
#include "ccs811.h"  // CCS811 library

#include "MHZ19.h"                                         // include main library
#include <SoftwareSerial.h>                                // Remove if using HardwareSerial or non-uno library compatable device

#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display

HDC1080JS tempsensor;

#define RX_PIN 10                                          // Rx pin which the MHZ19 Tx pin is attached to
#define TX_PIN 11                                          // Tx pin which the MHZ19 Rx pin is attached to
#define BAUDRATE 9600                                      // Native to the sensor (do not change)

MHZ19 myMHZ19;                                             // Constructor for MH-Z19 class
SoftwareSerial mySerial(RX_PIN, TX_PIN);                   // Uno example

//ENS210 ens210;
//CCS811 ccs811(D3); // nWAKE on D3

// Wiring for Nano: VDD to 3v3, GND to GND, SDA to A4, SCL to A5, nWAKE to 13
CCS811 ccs811(13); 

unsigned long getDataTimer = 0;                             // Variable to store timer interval


void setup() {
  // Enable LCD
  lcd.init();        // initialize the lcd 
  lcd.backlight();
  
  // Enable serial
  Serial.begin(115200);
  Serial.println("");
  Serial.println("setup: Starting CCS811 envdata demo");
  Serial.print("setup: library     version: "); Serial.println(CCS811_VERSION);

  // Enable I2C, e.g. for ESP8266 NodeMCU boards: VDD to 3V3, GND to GND, SDA to D2, SCL to D1, nWAKE to D3 (or GND)
  Serial.print("setup: I2C ");
  Wire.begin(); 
  Serial.println("ok");
  
  // Enable ENS210
  //Serial.print("setup: ENS210 ");
  bool ok;
  //ok= ens210.begin();
  //if( ok ) Serial.println("ok"); else Serial.println("FAILED");
  Serial.print("setup: HDC1080JS ");
  tempsensor = HDC1080JS();
  tempsensor.config();

  // Enable CCS811
  Serial.print("setup: CCS811 ");
  ccs811.set_i2cdelay(50); // Needed for ESP8266 because it doesn't handle I2C clock stretch correctly
  ok= ccs811.begin();
  if( ok ) Serial.println("ok"); else Serial.println("FAILED");

  // Start CCS811 (measure every 1 second)
  Serial.print("setup: CCS811 start ");
  ok= ccs811.start(CCS811_MODE_1SEC);
  if( ok ) Serial.println("ok"); else Serial.println("FAILED");

  mySerial.begin(BAUDRATE);                               // Uno example: Begin Stream with MHZ19 baudrate  
  myMHZ19.begin(mySerial);                                // *Important, Pass your Stream reference 
  myMHZ19.autoCalibration();                              // Turn auto calibration ON (disable with autoCalibration(false))
  myMHZ19.setRange(2000); 
  myMHZ19.calibrateZero();
  myMHZ19.setSpan(2000); 
  
  char myVersion[4];          
  myMHZ19.getVersion(myVersion);
  

  Serial.print("\nFirmware Version: ");
  for(byte i = 0; i < 4; i++)
  {
    Serial.print(myVersion[i]);
    if(i == 1)
      Serial.print(".");    
  }
   Serial.println("");

   Serial.print("Range: ");
   Serial.println(myMHZ19.getRange());   
   Serial.print("Background CO2: ");
   Serial.println(myMHZ19.getBackgroundCO2());
   Serial.print("Temperature Cal: ");
   Serial.println(myMHZ19.getTempAdjustment());
}



void loop() {
  
  
  tempsensor.readTempHumid();
  float temp = tempsensor.getTemp();
  float humid = tempsensor.getRelativeHumidity();
  int t_data, h_data;
  t_data = (int) (temp*100.0); //Convert to milliC and int
  h_data = (int) (humid*100.0); //Convert to milli%RH and int
  Serial.print( "HDC1080: " );
  Serial.print("T="); Serial.print(temp);     Serial.print(" C  ");
  Serial.print("H="); Serial.print(humid); Serial.print(" %RH  ");
  // Pass environmental data from ENS210 to CCS811
  ccs811.set_envdata210(t_data, h_data);
  Serial.print("  ");

  // Read CCS811
  uint16_t eco2, etvoc, errstat, raw;
  ccs811.read(&eco2,&etvoc,&errstat,&raw); 

  // Process CCS811
  Serial.print("CCS811: ");
  if( errstat==CCS811_ERRSTAT_OK ) {
    Serial.print("eco2=");  Serial.print(eco2);  Serial.print(" ppm  ");
    Serial.print("etvoc="); Serial.print(etvoc); Serial.print(" ppb  ");

    //lcd.clear();//Clean the screen
    lcd.setCursor(0,0);
    lcd.print("c="); lcd.print(eco2); lcd.print("ppm ");
    //lcd.setCursor(0,1);
    lcd.print("v="); lcd.print(etvoc); lcd.print("ppb");
  
  } else if( errstat==CCS811_ERRSTAT_OK_NODATA ) {
    Serial.print("waiting for (new) data");
  } else if( errstat & CCS811_ERRSTAT_I2CFAIL ) { 
    Serial.print("I2C error");
  } else {
    Serial.print( "error: " );
    Serial.print( ccs811.errstat_str(errstat) ); 
  }
  Serial.println();

  if (millis() - getDataTimer >= 2000)                    // Check if interval has elapsed (non-blocking delay() equivilant)
  {
      int CO2;                                            // Buffer for CO2

      /* note: getCO2() default is command "CO2 Unlimited". This returns the correct CO2 reading even 
      if below background CO2 levels or above range (useful to validate sensor). You can use the 
      usual documented command with getCO2(false) */

      CO2 = myMHZ19.getCO2();                             // Request CO2 (as ppm)

      if (myMHZ19.errorCode == RESULT_OK) 
      {
        Serial.print("CO2 (ppm): ");                      
        Serial.println(CO2);                                
    
        int8_t Temp;                                         // Buffer for temperature
        Temp = myMHZ19.getTemperature();                     // Request Temperature (as Celsius)
        Serial.print("Temperature (C): ");                  
        Serial.println(Temp);
    
        lcd.setCursor(0,1);
        lcd.print("c=");lcd.print(CO2);lcd.print("ppm ");
        lcd.print("T=");lcd.print(Temp);lcd.print("C");
      } else {
        Serial.println("Failed to recieve CO2 value - Error");
        Serial.print("Response Code: ");
        Serial.println(myMHZ19.errorCode);          // Get the Error Code value
      }
      getDataTimer = millis();                            // Update interval
  }

  // Wait
  delay(1000); 
}
